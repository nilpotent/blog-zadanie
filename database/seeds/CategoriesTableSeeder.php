<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {       
        
        DB::delete('delete from categories');
         
        $categories = [
            ['id' => 1, 'name' => 'Kategoria-1'],
            ['id' => 2, 'name' => 'Kategoria-2']
        ];
        DB::table('categories')->insert($categories);
    }
}
