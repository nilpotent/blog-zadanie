<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::delete('delete from comments');

        $comments = [
            ['id' => 1, 'message_id' => '1', 'author' => 'Janek', 'content' => 'News-1 is good', 'created_at' => '2017-03-13'],
            ['id' => 2, 'message_id' => '1', 'author' => 'John', 'content' => 'News-1 is not good', 'created_at' => '2017-03-13'],
            ['id' => 3, 'message_id' => '3', 'author' => 'Fiona', 'content' => 'News-3 is good', 'created_at' => '2017-03-13'],
        ];

        DB::table('comments')->insert($comments);
    }
}
