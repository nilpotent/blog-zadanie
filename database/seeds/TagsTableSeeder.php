<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->delete();
        DB::table('message_tag')->delete();

        $tags = [
            ['id' => 1, 'expression' => 'car', 'created_at' => '2016-05-02'],
            ['id' => 2, 'expression' => 'cat', 'created_at' => '2016-08-12'],
            ['id' => 3, 'expression' => 'dog', 'created_at' => '2017-01-23'],
            ['id' => 4, 'expression' => 'vw', 'created_at' => '2017-02-28'],
            ['id' => 5, 'expresion' => 'vehicle', 'created_at' => '2017-03-03']
        ];

        DB::table('tags')->insert($tags);

        $relation = [
            ['message_id' => 1, 'tag_id' => 1],
            ['message_id' => 3, 'tag_id' => 1],
            ['message_id' => 2, 'tag_id' => 2],
            ['message_id' => 2, 'tag_id' => 3],
            ['message_id' => 1, 'tag_id' => 5],
            ['message_id' => 1, 'tag_id' => 4]
        ];

        DB::table('message_tag')->insert($relation);
    }
}
