<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::delete('delete from messages');
        $messages = [
            ['id' => 1, 'category_id' => '1', 'title' => 'News-1', 'content' => 'Content of News-1', 'created_at' => '2017-03-13'],
            ['id' => 2, 'category_id' => '2', 'title' => 'News-2', 'content' => 'Content of News-2', 'created_at' => '2017-03-13'],
            ['id' => 3, 'category_id' => '2', 'title' => 'News-3', 'content' => 'Content of News-3', 'created_at' => '2017-03-13']
        ];
        DB::table('messages')->insert($messages);
    }
}
