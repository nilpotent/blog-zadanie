
@extends('layouts.app')

@section('header')
    Szczegóły aktualności
@endsection

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        <p>
            <strong>Aktualność </strong>
            {{$message->title}}
            <strong>z kategorii</strong>
            <i>{{$category->name}}</i>
            <strong>utworzono </strong>
            <span>{{date('d-m-Y', strtotime($message->created_at))}}</span>       
        </p>        
    </div>
    <div class="panel-body">
    {{$message->content}}
    </div>
    <div class="panel-footer">        
        @forelse($tags as $tag)
            <span class="badge">{{$tag}}</span>
        @empty
            <strong>Nie ma żadnych tagów</strong>
        @endforelse
    </div>
</div>

<hr />

<h2>Dodaj komentarz</h2>
{!!Form::open(['route' => 'comments.store', 'method' => 'post', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
        <div class="col-md-8">
            {!! Form::text('author', null, ['class' => 'form-control', 'placeholder' => 'autor']); !!}
        </div>                
    </div>
    <div class="form-group">
        <div class="col-md-8">
            {!! Form::textarea('content', null, ['class' => 'form-control', 'placeholder' => 'komentarz']); !!}
        </div>                
    </div>
    {!! Form::hidden('message_id', $message->id); !!}
    <div class="form-group">
        <div class="col-md-1">
            {!! Form::submit('Dodaj', ['class' => 'btn btn-default']) !!}
        </div>
    </div>    
{!!Form::close() !!}

<h2>Komentarze</h2>
@forelse($comments as $comment)
    <div class="media">  
        <div class="media-body">
            <h4 class="media-heading">{{$comment->author}} <small> {{date('d-m-Y', strtotime($comment->created_at))}}</small></h4>
            {{$comment->content}}
        </div>
    </div>
@empty
    <strong>Aktualnie nie ma żadnych komentarzy</strong>
@endforelse

@endsection
