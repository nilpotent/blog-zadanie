
@extends('layouts.app')

@section('header')
    Nowa aktualność
@endsection

@section('content')

{!!Form::open(['route' => 'messages.store', 'method' => 'post', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
        <div class="col-md-8">
            {!! Form::select('category_id', $categories, null, ['class' => 'form-control']); !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-8">        
            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'tytuł']); !!}
        </div>              
    </div>  
    <div class="form-group">
        <div class="col-md-8">
            {!! Form::textarea('content', null, ['class' => 'form-control', 'placeholder' => 'treść']); !!}
        </div>                                
    </div>
    <div class="form-group">
        <div class="col-md-8">
            {!! Form::text('tags', null, ['class' => 'form-control', 'placeholder' => 'tagi (wyrażenia odseparowane średnikiem)']); !!}
        </div>                                
    </div>
    <div class=form-group>
        <div class="col-md-1">
            {!! Form::submit('Dodaj', ['class' => 'btn btn-default']) !!}
        </div>
    </div>
{!!Form::close() !!}

@endsection