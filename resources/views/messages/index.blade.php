
@extends('layouts.app')

@section('header')
    Aktualności
@endsection

@section('content')

<div class="row">
    <div class="col-md-8">
        <ul class="list-unstyled">
            @foreach($messages as $message)
                <li>                                                
                    {!!Form::open(['route' => ['messages.destroy', $message->id], 'method' => 'delete', 'class'=> 'form-inline;']) !!}
                        <span class="messages-name">
                            {!! link_to_route('messages.show', $message->title, [$message->id], ['class' => 'btn btn-link']) !!}
                        </span>
                        <span class="messages-date">{{ date('d-m-Y', strtotime($message->created_at)) }}</span>                
                        {!! link_to_route('messages.edit', 'Edytuj', [$message->id], ['class' => 'btn btn-default']) !!}
                        {!! Form::submit('Usuń', ['class' => 'btn btn-danger']) !!}       
                        <hr />         
                    {!!Form::close() !!}                        
                </li>
            @endforeach    
        </ul>
        {{$messages->links()}}
    </div>
    <div class="col-md-4">
    <div class="panel panel-default">
    <div class="panel-heading">
        <h2>Tagi</h2>
    </div>
    <div class="panel-body">
        @forelse($tags as $tag)
            {!! link_to_route('messages.filter', $tag->expression, [$tag->id], ['class' => 'btn btn-link']) !!}            
        @empty
            <strong>Nie ma żadnych tagów</strong>
        @endforelse
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2>Wybrane Tagi</h2>
    </div>
    <div class="panel-body">
        @if(count($choosenTags) > 0)
            {!! link_to_route('messages.filter', 'Wyczyść', [null], ['class' => 'btn btn-link']) !!}
        @endif
        @forelse($choosenTags as $tag)
            {!! link_to_route('messages.filter', $tag->expression, [$tag->id], ['class' => 'btn btn-link']) !!}            
        @empty
            <strong>Tagi nie zostały wybrane.</strong>
        @endforelse
    </div>
    </div>
</div>


</div>

@endsection