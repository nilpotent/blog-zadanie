
@extends('layouts.app')

@section('header')
    Zmień aktualność
@endsection

@section('content')

{!!Form::open(['route' => ['messages.update', $message->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
    <div class="form-group">
        <div class="col-md-8">
            {!! Form::select('category_id', $categories, $message->category_id, ['class' => 'form-control']); !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-8">        
            {!! Form::text('title', $message->title, ['class' => 'form-control', 'placeholder' => 'tytuł']); !!}
        </div>              
    </div>  
    <div class="form-group">
        <div class="col-md-8">
            {!! Form::textarea('content', $message->content, ['class' => 'form-control', 'placeholder' => 'treść']); !!}
        </div>                                
    </div>
    <div class="form-group">
        <div class="col-md-8">
            {!! Form::text('tags', $tags, ['class' => 'form-control', 'placeholder' => 'tagi (wyrażenia odseparowane średnikiem)']); !!}
        </div>                                
    </div>
    <div class=form-group>
        <div class="col-md-1">
            {!! Form::submit('Zmień', ['class' => 'btn btn-default']) !!}
        </div>
    </div>
{!!Form::close() !!}
@endsection