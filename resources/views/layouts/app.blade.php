
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>BlogApp</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    {{Html::style('css/style.css')}}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>    
    <div class="container">

      <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>              
              {!! link_to_route('messages.index', 'BlogApp', null, ['class' => 'navbar-brand']) !!}
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li>{!! link_to_route('messages.create', 'Nowa aktualność', null, ['class' => 'btn btn-link']) !!}</li>
                <li>{!! link_to_route('messages.index', 'Aktualności', null, ['class' => 'btn btn-link']) !!}</li>
                <li>{!! link_to_route('categories.index', 'Kategorie', null, ['class' => 'btn btn-link']) !!}</li>              
            </div>
          </div>
        </nav>
  
        <div class="row">
          @if (Session::has('message'))		
            <div class="alert alert-info">			
              <p>{{ Session::get('message') }}</p>		
            </div>
          @endif
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
        </div>
        
        <div class = "page-header">
          <h1>
            @yield('header')             
          </h1>   
        </div>

        
        @yield('content')
    </div>
  </body>
</html>