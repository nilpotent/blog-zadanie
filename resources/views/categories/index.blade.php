
@extends('layouts.app')

@section('header')
    Kategorie
@endsection

@section('content')

<div class="row">
<div class="col-md-9">
<ul class="list-unstyled">
    @foreach( $categories as $category )
        <li>                                                
            {!!Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete', 'class'=> 'form-inline;']) !!}
                <span class="categories-name">{{ $category->name }}</span>                
                {!! link_to_route('categories.edit', 'Edytuj', [$category->id], ['class' => 'btn btn-default']) !!}
                {!! Form::submit('Usuń', ['class' => 'btn btn-danger']) !!}       
                <hr />         
            {!!Form::close() !!}                        
        </li>
    @endforeach    
</ul>
</div>

<div class="col-md-3">
<h2>Nowa kategoria</h2>
{!!Form::open(['route' => 'categories.store', 'method' => 'post', 'class' => 'form-inline']) !!}
    <div class="form-group">        
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'dodaj kategorię']); !!}                                        
    </div>
    {!! Form::submit('Dodaj', ['class' => 'btn btn-default']) !!}
{!!Form::close() !!}
</div>
</div>
@endsection