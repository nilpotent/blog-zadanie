@extends('layouts.app')

@section('header')
    Zmień kategorię
@endsection

@section('content')

{!!Form::open(['route' => ['categories.update', $category->id], 'method' => 'put', 'class' => 'form-inline']) !!}
    <div class="form-group">        
        {!! Form::text('name', $category->name, ['class' => 'form-control']); !!}        
    </div>
    {!! Form::submit('Zmień', ['class' => 'btn btn-default']) !!}    
{!!Form::close() !!}
@endsection