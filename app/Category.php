<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    public function messages() 
    {
        return $this->hasMany(Message::class);
    }
}
