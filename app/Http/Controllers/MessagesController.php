<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Message;
use App\Category;
use App\Tag;

class MessagesController extends Controller
{
    public function index() 
    {        
        $tags = Tag::get();        
        $tagsCookie = $this->getTagsFromCookie();   

        $choosenTags = array_map(function($item) use ($tags){
            return $tags->where('id', $item)->first();
        }, $tagsCookie);

        $query = DB::table('messages')
            ->leftJoin('message_tag', 'id', '=', 'message_id')
            ->orWhere(function($query) use ($tagsCookie) {
                array_walk($tagsCookie, function($tag_id) use ($query) {
                    $query->orWhere('tag_id', '=', $tag_id);
                });                
            });

        $tags = $tags->diff($choosenTags);

        $messages = $query->select(['id', 'title', 'created_at'])
            ->groupBy(['id', 'title', 'created_at'])
            ->paginate(4);  

        return view('messages.index', compact('messages', 'tags', 'choosenTags'));
    }

    private function refreshCookie($tag_id = null) {
        if($tag_id === null) 
        {
            \Cookie::queue(\Cookie::forever('tags', ''));
            return [];
        }
        $cookie = \Cookie::get('tags') == null ? '' : \Cookie::get('tags');           
        $temp = explode(';', $cookie);
        if(array_search($tag_id, $temp)) {
            $temp = array_diff($temp, [$tag_id]);
        }
        else {
            $temp[] = $tag_id;
        }
        $cookie = implode(';', $temp);
        \Cookie::queue(\Cookie::forever('tags', $cookie));
        return array_slice($temp, 1);
    }

    private function getTagsFromCookie() {
        $cookie = \Cookie::get('tags') == null ? '' : \Cookie::get('tags');           
        $temp = explode(';', $cookie);
        return array_slice($temp, 1);
    }

    public function filter($tag_id = null)
    {
        $this->refreshCookie($tag_id);
        return Redirect::route('messages.index');
    }    

    public function create() 
    {
        $categories = $this->getCategoriesToList();
        return view('messages.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'category_id' => 'required'
        ], [
            'title.required' => 'Nagłówek aktualności nie może być pusty!',
            'content.required' => 'Treść aktualności nie może być pusta!',
            'category_id.required' => 'Kategoria musi być wybrana!'
        ]);
        $input = Input::except('tags');        
        $input['created_at'] = new \DateTime();
        $message = Message::create($input);       

        $ids = $this->extractTags();
        if(count($ids) > 0) 
        {
            $message->tags()->attach($ids);
        }

        return Redirect::route('messages.index')
            ->with('message', 'Aktualność została utworzona');
    }

    public function destroy(Message $message) 
    {
        $message->delete();
        return Redirect::route('messages.index')
            ->with('message', 'Aktualność została usunięta');
    }

    public function update(Request $request, Message $message)
    {        
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'category_id' => 'required'
        ], [
            'title.required' => 'Nagłówek aktualności nie może być pusty!',
            'content.required' => 'Treść aktualności nie może być pusta!',
            'category_id.required' => 'Kategoria musi być wybrana!'
        ]);

        $input = Input::except('_method', 'tags');
        $message->update($input);

        $ids = $this->extractTags();
        $message->tags()->sync($ids);

        return Redirect::route('messages.index')
            ->with('message', 'Kategoria została zmieniona');
    }

    public function edit(Message $message)
    {
        $categories = $this->getCategoriesToList();
        $expressions = $this->getTags($message);
        $tags = implode(';', $expressions);       
        return view('messages.edit', compact('message', 'categories', 'tags'));
    }

    public function show(Message $message) 
    {
        $category = $message->category()->first();
        $comments = $message->comments()->get();        
        $tags = $this->getTags($message);
        return view('messages.show', compact('message', 'category', 'comments', 'tags'));
    }

    private function getCategoriesToList() {
        return Category::all()->mapWithKeys(function($item){
            return [$item['id'] => $item['name']];
        });
    }

    private function extractTags() {
        if(Input::get('tags') != null) 
        {
            $tags = explode(';', Input::get('tags'));
            $ids = array_map(function($tag) {
                $entity = Tag::firstOrCreate(['expression' => $tag]);
                return $entity->id;
            }, $tags);
            return $ids;            
        }
        return [];
    }

    private function getTags(Message $message) {
        return $message->tags()
            ->get()
            ->flatMap(function($tag) {
                return [$tag->expression];
            })
            ->all();
    }  
}
