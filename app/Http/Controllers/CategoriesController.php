<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Category;

class CategoriesController extends Controller
{
    public function index() 
    {
        $categories = Category::all();
        return view('categories.index', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',            
        ], [
            'name.required' => 'Nazwa kategorii nie może być pusta!',            
        ]);
        $input = Input::all();
        Category::create($input);
        return Redirect::route('categories.index')
            ->with('message', 'Kategoria została utworzona');
    }

    public function destroy(Category $category) 
    {
        $category->delete();
        return Redirect::route('categories.index')
            ->with('message', 'Kategoria została usunięta');
    }

    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'name' => 'required',            
        ], [
            'name.required' => 'Nazwa kategorii nie może być pusta!',            
        ]);
        $input = array_except(Input::all(), '_method');
        $category->update($input);
        return Redirect::route('categories.index')
            ->with('message', 'Kategoria została zmieniona');
    }

    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }
}
