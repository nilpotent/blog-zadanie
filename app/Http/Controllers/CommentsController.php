<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Comment;

class CommentsController extends Controller
{    
    public function store(Request $request)
    {
        $this->validate($request, [
            'author' => 'required',
            'content' => 'required'            
        ], [
            'author.required' => 'Autor komentarza nie może być anonimowy!',
            'content.required' => 'Treść komentarza nie może być pusta!'            
        ]);
        $input = Input::all();
        $input['created_at'] = new \Datetime();
        Comment::create($input);
        $message_id = Input::get('message_id');
        return Redirect::route('messages.show', ['id' => $message_id])
            ->with('message', 'Kamentarz został utworzony');
    }    
}
