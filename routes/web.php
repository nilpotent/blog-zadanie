<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MessagesController@index');

Route::get('messages/filter/{tag_id?}', 'MessagesController@filter')->name('messages.filter');

Route::model('categories', App\Category::class);
Route::model('messages', App\Message::class);

Route::bind('categories', function($value, $route) {
    return App\Category::whereId($value)->first();
});

Route::bind('messages', function($value, $route) {
    return App\Message::whereId($value)->first();
});

Route::resource('categories', 'CategoriesController');
Route::resource('messages', 'MessagesController');
Route::resource('comments', 'CommentsController');

